#!/bin/bash

sudo apt-get install -y vim screen pinfo mc htop python python-pip build-essential git zsh
sudo pip install fabric pexpect

PASSWORD=$1

pushd $HOME

git clone https://lexhung:$PASSWORD@bitbucket.org/lexhung/ubuntu-config.git .etc
git clone https://github.com/robbyrussell/oh-my-zsh.git .zsh-extension

python .etc/_bin/prosync.py

chsh /usr/bin/zsh
popd